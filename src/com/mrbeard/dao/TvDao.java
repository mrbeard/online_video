package com.mrbeard.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import com.mrbeard.dao.interfaces.IVideoDao;
import com.mrbeard.dao.interfaces.IVideoItemDao;
import com.mrbeard.entity.Animation;
import com.mrbeard.entity.Movie;
import com.mrbeard.entity.Tv;
import com.mrbeard.entity.Tv_PlayItem;
import com.mrbeard.entity.Variety;
import com.mrbeard.util.VideoUtil;

@Component
public class TvDao implements IVideoDao<Tv>, IVideoItemDao<Tv_PlayItem> {

	// 注入
	@Resource
	private SessionFactory sessionFactory;

	@Override
	public int getTotalNumber() {
		// TODO Auto-generated method stub
		// 得到结果集
		Query query = sessionFactory.getCurrentSession().createQuery("from Tv");

		// 得到滚动的结果集
		ScrollableResults scroll = query.scroll();

		// 滚到最后一行
		scroll.last();
		// 得到滚到的记录数，即总记录数
		int totalCount = scroll.getRowNumber() + 1;
		return totalCount;
	}

	@Override
	public List<Tv> getVideoList(int currentPage, int number) {
		// 得到结果集
		Query query = sessionFactory.getCurrentSession().createQuery("from Tv");

		// 得到滚动的结果集
		ScrollableResults scroll = query.scroll();

		// 滚到最后一行
		scroll.last();
		// 得到滚到的记录数，即总记录数
		int totalCount = scroll.getRowNumber() + 1;

		// 设置分页参数
		query.setFirstResult(currentPage);
		query.setMaxResults(number);

		// 查询
		return query.list();
	}

	@Override
	public Tv getVideo(Serializable id) {
		return (Tv) sessionFactory.getCurrentSession().get(Tv.class, id);
	}

	@Override
	public Map<Integer, List<String>> getPlayItem(Tv_PlayItem playItem) {
		List<String> firstUrl = VideoUtil.getUrlArry(playItem.getPlayUrls());
		Map<Integer, List<String>> second = new HashMap<>();
		for (int i = 0; i < firstUrl.size(); i++) {
			second.put(i, VideoUtil.getUrlList(firstUrl.get(i)));
		}
		return second;
	}

	@Override
	public List<String> getOriginList(Tv_PlayItem playItem) {
		// TODO Auto-generated method stub
		return VideoUtil.getOriginList(playItem.getOrigin());
	}

	@Override
	public List<Tv> getVideoByType(String type, String time, String region, int currentPage, int number) {
		// 判断
		if (type != null) {
			if (time != null) {
				if (region != null) {
					// 得到结果集
					Query query = sessionFactory.getCurrentSession()
							.createQuery("from Tv where type like :type and  time like:time and region like:region ");
					query.setParameter("type", "%" + type + "%");
					query.setParameter("time", "%" + time + "%");
					query.setParameter("region", "%" + region + "%");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				} else {
					// 得到结果集
					Query query = sessionFactory.getCurrentSession()
							.createQuery("from Tv where type like :type and  time like:time ");
					query.setParameter("type", "%" + type + "%");
					query.setParameter("time", "%" + time + "%");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				}

			} else {
				if (region != null) {
					// 得到结果集
					Query query = sessionFactory.getCurrentSession()
							.createQuery("from Tv where type like :type and  region like:region ");
					query.setParameter("type", "%" + type + "%");
					query.setParameter("region", "%" + region + "%");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				} else {
					// 得到结果集
					Query query = sessionFactory.getCurrentSession().createQuery("from Tv where type like :type ");
					query.setParameter("type", "%" + type + "%");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				}
			}

		} else {
			if (time != null) {
				if (region != null) {
					// 得到结果集
					Query query = sessionFactory.getCurrentSession()
							.createQuery("from Tv where time like :time and  region like:region ");
					query.setParameter("time", "%" + time + "%");
					query.setParameter("region", "%" + region + "%");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				} else {
					// 得到结果集
					Query query = sessionFactory.getCurrentSession().createQuery("from Tv where time like :time ");
					query.setParameter("time", "%" + time + "%");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				}
			} else {
				if (region != null) {
					// 得到结果集
					Query query = sessionFactory.getCurrentSession().createQuery("from Tv where region like :region ");
					query.setParameter("region", "%" + region + "%");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				} else {
					// 得到结果集
					Query query = sessionFactory.getCurrentSession().createQuery("from Tv");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				}
			}
		}

	}

	@Override
	public List<Tv> getVideo(String title) {
		Query query = sessionFactory.getCurrentSession().createQuery("from Tv where title like :title");
		query.setParameter("title", "%" + title + "%");
		List<Tv> tv_List = query.list();
		return tv_List;
	}

}
