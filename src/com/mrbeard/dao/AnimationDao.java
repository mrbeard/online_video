package com.mrbeard.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import com.mrbeard.dao.interfaces.IVideoDao;
import com.mrbeard.dao.interfaces.IVideoItemDao;
import com.mrbeard.entity.Animation;
import com.mrbeard.entity.Animation_PlayItem;
import com.mrbeard.entity.Movie;
import com.mrbeard.entity.Tv;
import com.mrbeard.entity.Tv_PlayItem;
import com.mrbeard.entity.Variety;
import com.mrbeard.util.VideoUtil;

@Component
public class AnimationDao implements IVideoDao<Animation> ,IVideoItemDao<Animation_PlayItem>{


	//注入
	@Resource
	private SessionFactory sessionFactory;

	@Override
	public int getTotalNumber() {
		// TODO Auto-generated method stub
		// 得到结果集
		Query query = sessionFactory.getCurrentSession().createQuery("from Animation");

		// 得到滚动的结果集
		ScrollableResults scroll = query.scroll();

		// 滚到最后一行
		scroll.last();
		// 得到滚到的记录数，即总记录数
		int totalCount = scroll.getRowNumber() + 1;
		return totalCount;
	}

	@Override
	public List<Animation> getVideoList(int currentPage, int number) {
		// 得到结果集
		Query query = sessionFactory.getCurrentSession().createQuery("from Animation");

		// 得到滚动的结果集
		ScrollableResults scroll = query.scroll();

		// 滚到最后一行
		scroll.last();
		// 得到滚到的记录数，即总记录数
		int totalCount = scroll.getRowNumber() + 1;

		// 设置分页参数
		query.setFirstResult(currentPage);
		query.setMaxResults(number);

		// 查询
		return query.list();
	}

	

	

	@Override
	public Animation getVideo(Serializable id) {
		return (Animation) sessionFactory.getCurrentSession().get(Animation.class, id);
	}

	@Override
	public List<Animation> getVideo(String title) {
		Query  query =  sessionFactory.getCurrentSession().createQuery("from Animation where title like :title");
		query.setParameter("title", "%"+title+"%");
		List<Animation> animation_List =  query.list();
		return animation_List;
	}

	

	@Override
	public Map<Integer, List<String>> getPlayItem(Animation_PlayItem playItem) {
		List<String> firstUrl = VideoUtil.getUrlArry(playItem.getPlayUrls());
		Map<Integer ,List<String>> second = new HashMap<>();
		for(int i =0 ; i< firstUrl.size();i++) {
			second.put(i, VideoUtil.getUrlList(firstUrl.get(i)));
		}
		return second;
	}

	@Override
	public List<String> getOriginList(Animation_PlayItem playItem) {
		// TODO Auto-generated method stub
		return VideoUtil.getOriginList(playItem.getOrigin());
	}

	@Override
	public List<Animation> getVideoByType(String type, String time, String region, int currentPage, int number) {
		//判断
		if(type!=null) {
			if(time!=null) {
				if(region!=null) {
					// 得到结果集
					Query  query =  sessionFactory.getCurrentSession().createQuery("from Animation where type like :type and  time like:time and region like:region ");
					query.setParameter("type", "%"+type+"%");
					query.setParameter("time", "%"+time+"%");
					query.setParameter("region", "%"+region+"%");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				}else {
					// 得到结果集
					Query  query =  sessionFactory.getCurrentSession().createQuery("from Animation where type like :type and  time like:time ");
					query.setParameter("type", "%"+type+"%");
					query.setParameter("time", "%"+time+"%");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				}
				
			}else {
				if(region!=null) {
					// 得到结果集
					Query  query =  sessionFactory.getCurrentSession().createQuery("from Animation where type like :type and  region like:region ");
					query.setParameter("type", "%"+type+"%");
					query.setParameter("region", "%"+region+"%");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				}else {
					// 得到结果集
					Query  query =  sessionFactory.getCurrentSession().createQuery("from Animation where type like :type ");
					query.setParameter("type", "%"+type+"%");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				}
			}
			
		}else {
			if(time!=null){
				if(region!=null){
					// 得到结果集
					Query  query =  sessionFactory.getCurrentSession().createQuery("from Animation where time like :time and  region like:region ");
					query.setParameter("time", "%"+time+"%");
					query.setParameter("region", "%"+region+"%");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				}else{
					// 得到结果集
					Query  query =  sessionFactory.getCurrentSession().createQuery("from Animation where time like :time ");
					query.setParameter("time", "%"+time+"%");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				}
			}else{
				if(region!=null){
					// 得到结果集
					Query  query =  sessionFactory.getCurrentSession().createQuery("from Animation where region like :region ");
					query.setParameter("region", "%"+region+"%");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				}else{
					// 得到结果集
					Query  query =  sessionFactory.getCurrentSession().createQuery("from Animation");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				}
			}
		}
		
		
	}

	
	

}
