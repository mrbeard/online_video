package com.mrbeard.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import com.mrbeard.dao.interfaces.IVideoDao;
import com.mrbeard.dao.interfaces.IVideoItemDao;
import com.mrbeard.entity.Animation;
import com.mrbeard.entity.Movie;
import com.mrbeard.entity.Tv;
import com.mrbeard.entity.Tv_PlayItem;
import com.mrbeard.entity.Variety;
import com.mrbeard.entity.Variety_PlayItem;
import com.mrbeard.util.VideoUtil;

@Component
public class VarietyDao implements IVideoDao<Variety> ,IVideoItemDao<Variety_PlayItem>{


	//注入
	@Resource
	private SessionFactory sessionFactory;

	@Override
	public int getTotalNumber() {
		// TODO Auto-generated method stub
		// 得到结果集
		Query query = sessionFactory.getCurrentSession().createQuery("from Variety");

		// 得到滚动的结果集
		ScrollableResults scroll = query.scroll();

		// 滚到最后一行
		scroll.last();
		// 得到滚到的记录数，即总记录数
		int totalCount = scroll.getRowNumber() + 1;
		return totalCount;
	}

	@Override
	public List<Variety> getVideoList(int currentPage, int number) {
		// 得到结果集
		Query query = sessionFactory.getCurrentSession().createQuery("from Variety");

		// 得到滚动的结果集
		ScrollableResults scroll = query.scroll();

		// 滚到最后一行
		scroll.last();
		// 得到滚到的记录数，即总记录数
		int totalCount = scroll.getRowNumber() + 1;

		// 设置分页参数
		query.setFirstResult(currentPage);
		query.setMaxResults(number);

		// 查询
		return query.list();
	}

	

	

	@Override
	public Variety getVideo(Serializable id) {
		return (Variety) sessionFactory.getCurrentSession().get(Variety.class, id);
	}

	@Override
	public List<Variety> getVideo(String title) {
		Query  query =  sessionFactory.getCurrentSession().createQuery("from Variety where title like :title");
		query.setParameter("title", "%"+title+"%");
		List<Variety> variety_List =  query.list();
		return variety_List;
	}

	

	@Override
	public Map<Integer, List<String>> getPlayItem(Variety_PlayItem playItem) {
		List<String> firstUrl = VideoUtil.getUrlArry(playItem.getPlayUrls());
		Map<Integer ,List<String>> second = new HashMap<>();
		for(int i =0 ; i< firstUrl.size();i++) {
			second.put(i, VideoUtil.getUrlList(firstUrl.get(i)));
		}
		return second;
	}

	@Override
	public List<String> getOriginList(Variety_PlayItem playItem) {
		// TODO Auto-generated method stub
		return VideoUtil.getOriginList(playItem.getOrigin());
	}

	
	@Override
	public List<Variety> getVideoByType(String type, String time, String region, int currentPage, int number) {
		//判断
		if(type!=null) {
			if(time!=null) {
				if(region!=null) {
					// 得到结果集
					Query  query =  sessionFactory.getCurrentSession().createQuery("from Variety where type like :type and  time like:time and region like:region ");
					query.setParameter("type", "%"+type+"%");
					query.setParameter("time", "%"+time+"%");
					query.setParameter("region", "%"+region+"%");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				}else {
					// 得到结果集
					Query  query =  sessionFactory.getCurrentSession().createQuery("from Variety where type like :type and  time like:time ");
					query.setParameter("type", "%"+type+"%");
					query.setParameter("time", "%"+time+"%");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				}
				
			}else {
				if(region!=null) {
					// 得到结果集
					Query  query =  sessionFactory.getCurrentSession().createQuery("from Variety where type like :type and  region like:region ");
					query.setParameter("type", "%"+type+"%");
					query.setParameter("region", "%"+region+"%");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				}else {
					// 得到结果集
					Query  query =  sessionFactory.getCurrentSession().createQuery("from Variety where type like :type ");
					query.setParameter("type", "%"+type+"%");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				}
			}
			
		}else {
			if(time!=null){
				if(region!=null){
					// 得到结果集
					Query  query =  sessionFactory.getCurrentSession().createQuery("from Variety where time like :time and  region like:region ");
					query.setParameter("time", "%"+time+"%");
					query.setParameter("region", "%"+region+"%");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				}else{
					// 得到结果集
					Query  query =  sessionFactory.getCurrentSession().createQuery("from Variety where time like :time ");
					query.setParameter("time", "%"+time+"%");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				}
			}else{
				if(region!=null){
					// 得到结果集
					Query  query =  sessionFactory.getCurrentSession().createQuery("from Variety where region like :region ");
					query.setParameter("region", "%"+region+"%");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				}else{
					// 得到结果集
					Query  query =  sessionFactory.getCurrentSession().createQuery("from Variety");

					// 得到滚动的结果集
					ScrollableResults scroll = query.scroll();

					// 滚到最后一行
					scroll.last();
					// 得到滚到的记录数，即总记录数
					int totalCount = scroll.getRowNumber() + 1;

					// 设置分页参数
					query.setFirstResult(currentPage);
					query.setMaxResults(number);

					// 查询
					return query.list();
				}
			}
		}
		
		
	}

	

	

}
