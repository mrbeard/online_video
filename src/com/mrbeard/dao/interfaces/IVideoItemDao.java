package com.mrbeard.dao.interfaces;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.mrbeard.entity.Animation;
import com.mrbeard.entity.Animation_PlayItem;
import com.mrbeard.entity.Movie;
import com.mrbeard.entity.Tv;
import com.mrbeard.entity.Tv_PlayItem;
import com.mrbeard.entity.Variety;

/**
 *  视频处理dao
 * @author 胡彬
 *
 */
public interface  IVideoItemDao<T> {

	
	/**
	 * 获取url集合
	 * @param playItem
	 * @return
	 */
	Map<Integer,List<String>> getPlayItem(T playItem);
	
	/**
	 * 获取来源集合
	 * @param listPlayItem
	 * @return
	 */
	List<String> getOriginList(T playItem);
	
}
