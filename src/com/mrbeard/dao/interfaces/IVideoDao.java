package com.mrbeard.dao.interfaces;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import com.mrbeard.entity.Tv_PlayItem;

/**
 *  视频处理dao
 * @author 胡彬
 *
 */
public interface  IVideoDao<T> {

	/**
	 * 获取总记录数
	 * @return
	 */
	int getTotalNumber();
	
	/**
	 *  分页查询
	 * @param currentPage 当前页
	 * @param number 每页显示数目
	 * @return
	 */
	List<T> getVideoList(int currentPage,int number);
	
	/**
	 *  获取视频
	 * @param id 视频id
	 * @return
	 */
	T getVideo(Serializable id);
	

	
	/**
	 * 通过模糊搜索获取视频
	 * @param title
	 * @return
	 */
	List<T> getVideo(String title);
	/**
	 * 通过三个属性查找
	 * @param type
	 * @param time
	 * @param region
	 * @return
	 */
	List<T> getVideoByType(String type ,String time ,String region,int currentPage,int number);
}
