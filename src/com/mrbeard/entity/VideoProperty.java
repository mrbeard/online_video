package com.mrbeard.entity;

public class VideoProperty {
	private String type;
	private String time;
	private String region;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	@Override
	public String toString() {
		return "VideoProperty [type=" + type + ", time=" + time + ", region=" + region + "]";
	}
	
	

}
