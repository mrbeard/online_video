package com.mrbeard.entity;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.map.HashedMap;

/**
 * 综艺
 * @author 胡彬
 *
 */
public class Variety {
	
	private int id;
	
	/**
	 * 标题
	 */
	private String title;
	
	/**
	 * 主演
	 */
	private String stars; 
	/**
	 * 类型
	 */
	private String type; 
	/**
	 * 导演
	 */
	private String director; 
	/**
	 * 地区
	 */
	private String region; 
	/**
	 * 年份
	 */
	private String time; 
	
	/**
	 * 图片url
	 */
	private String imgUrl; 
	
	
	/**
	 * 评论
	 */
	private String comment;
	
	/**
	 * 播放地址url
	 */
	private Variety_PlayItem playItem;


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getStars() {
		return stars;
	}


	public void setStars(String stars) {
		this.stars = stars;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getDirector() {
		return director;
	}


	public void setDirector(String director) {
		this.director = director;
	}


	public String getRegion() {
		return region;
	}


	public void setRegion(String region) {
		this.region = region;
	}


	public String getTime() {
		return time;
	}


	public void setTime(String time) {
		this.time = time;
	}


	public String getImgUrl() {
		return imgUrl;
	}


	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}


	

	public Variety_PlayItem getPlayItem() {
		return playItem;
	}


	public void setPlayItem(Variety_PlayItem playItem) {
		this.playItem = playItem;
	}


	public String getComment() {
		return comment;
	}


	public void setComment(String comment) {
		this.comment = comment;
	}


	@Override
	public String toString() {
		return "Variety [id=" + id + ", title=" + title + ", stars=" + stars + ", type=" + type + ", director="
				+ director + ", region=" + region + ", time=" + time + ", imgUrl=" + imgUrl + ", comment=" + comment
				+ ", playItem=" + playItem + "]";
	}


	
	

}
