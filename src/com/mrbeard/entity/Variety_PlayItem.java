package com.mrbeard.entity;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Variety_PlayItem {


	/**
	 * ID
	 */
	private int  id; 
	/**
	 * 视频来源
	 */
	private String origin;
	
	/**
	 * 视频播放地址数组
	 */
	private String  playUrls;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getPlayUrls() {
		return playUrls;
	}

	public void setPlayUrls(String playUrls) {
		this.playUrls = playUrls;
	}

	@Override
	public String toString() {
		return "Variety_PlayItem [id=" + id + ", origin=" + origin + ", playUrls=" + playUrls + "]";
	}

	
	
	

}
