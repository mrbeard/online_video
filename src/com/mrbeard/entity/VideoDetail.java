package com.mrbeard.entity;

import java.util.List;
import java.util.Map;

public class VideoDetail {
	//标题
	private String title;
	//视频图片
	private String imgUrl;
	//地区
	private String region;
	//类型
	private String type;
	//时间
	private String time;
	//导演
	private String director;
	//主演
	private String starts;
	//总集数
	private String totalNumber;
	//剧情
	private String comment;
	//来源
	private List<String> origin;
	//视频url
	private Map<Integer , List<String>> urls;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public String getStarts() {
		return starts;
	}
	public void setStarts(String starts) {
		this.starts = starts;
	}
	public String getTotalNumber() {
		return totalNumber;
	}
	public void setTotalNumber(String totalNumber) {
		this.totalNumber = totalNumber;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public List<String> getOrigin() {
		return origin;
	}
	public void setOrigin(List<String> origin) {
		this.origin = origin;
	}
	public Map<Integer, List<String>> getUrls() {
		return urls;
	}
	public void setUrls(Map<Integer, List<String>> urls) {
		this.urls = urls;
	}
	@Override
	public String toString() {
		return "VideoDetail [title=" + title + ", imgUrl=" + imgUrl + ", region=" + region + ", type=" + type
				+ ", time=" + time + ", director=" + director + ", starts=" + starts + ", totalNumber=" + totalNumber
				+ ", comment=" + comment + ", origin=" + origin + ", urls=" + urls + "]";
	}
	

}
