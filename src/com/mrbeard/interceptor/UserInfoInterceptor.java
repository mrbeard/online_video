package com.mrbeard.interceptor;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.ActionProxy;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

/**
 * 自定义验证登陆拦截器
 * @author 胡彬
 *
 */
public class UserInfoInterceptor extends AbstractInterceptor{

	
	@Override
	public String intercept(ActionInvocation invocat) throws Exception {
		//获取action的代理对象
		ActionProxy actionProxy = invocat.getProxy();
		//获取当前执行的方法名
		String methodName = actionProxy.getMethod();
		
		//获取actioncontext对象
		ActionContext actionContext = invocat.getInvocationContext();
		
		//判断是否是login方法，如果不是则验证session
		if(!"login".equals(methodName)) {
			//获取当前登录的用户
			Object object =  actionContext.getSession().get("loginInfo");
			if(object == null) {
				//没有登陆
				return "loginFiled";
			}else {
				//已经登陆
				return invocat.invoke();
			}
		}else {
			//正在登陆
			return invocat.invoke();
		}
	}

}
