package com.mrbeard.interceptor;

import org.hibernate.Session;

import com.mrbeard.util.HibernateUtils;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

/**
 * 自定义session拦截器
 * @author 胡彬
 *
 */
public class SessionInterceptor extends AbstractInterceptor {

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		try {
			//创建session
			Session session = HibernateUtils.getSession();
			
			//开启事务
			session.beginTransaction();
			
					
			//执行Action
			String result = invocation.invoke();
			
			//提交事务
			session.getTransaction().commit();
			
			//返回结果视图
			return result;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "error";
		}
	}

}
