package com.mrbeard.util;

import java.util.ArrayList;
import java.util.List;

/**
 * 用于视频转化工具类
 * @author 胡彬
 *
 */
public class VideoUtil {

	/**
	 * 第一次解析将视频url String解析成[]
	 * @param urlContent
	 * @return
	 */
	public static List<String> getUrlArry(String urlContent){
		String[] url  = urlContent.split("#");
		List<String> urlList = new ArrayList<String>();
		for(String urlC : url) {
			urlList.add(urlC);
		}
		return urlList;
		
	}
	
	/**
	 * 第二次解析
	 * 将视频url String解析成List
	 * @param urlContent
	 * @return
	 */
	public static List<String> getUrlList(String urlContent){
		String[] url  = urlContent.split("@");
		List<String> urlList = new ArrayList<String>();
		for(String urlC : url) {
			urlList.add(urlC);
		}
		return urlList;
		
	}
	/**
	 * 将来源内容解析成集合
	 * @param origin
	 * @return
	 */
	
	public static List<String> getOriginList(String origin){
		List<String> urlList = new ArrayList<String>();
		String [] arryOrigin = origin.split("@");
		for(String originContent : arryOrigin) {
			urlList.add(originContent);
		}
		return urlList;
	}
}
