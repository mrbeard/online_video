package com.mrbeard.action;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.mrbeard.entity.Movie;
import com.mrbeard.entity.Tv;
import com.mrbeard.entity.VideoDetail;
import com.mrbeard.service.MovieService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

@Component
public class MovieAction extends ActionSupport {

	@Resource
	private MovieService movieService;

	/**
	 * 页码
	 */
	private int page;
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	
	
	
	private String title;
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * 访问主页
	 * 
	 * @return
	 */
	public String index() {
		// 查询前30条
		List<Movie> movieList = movieService.getVideoList(0, 30);
		// 将数据放到request
		Map<String, Object> request = ActionContext.getContext().getContextMap();
		request.put("movieList", movieList);
		request.put("page", 0);
		return "index";
	}

	/**
	 * 查询并到结果页
	 * 
	 * @return
	 */
	public String item() {
		// 查询
		if (!title.isEmpty()) {
			List<Movie> result = movieService.getVideo(title);
			Movie movie = result.get(0);// 获取到电视剧
			System.out.println(movie.getTitle());
			// 获取到playItemList

			Map<Integer, List<String>> urlMap = movieService.getPlayItem(movie.getPlayItem());

			List<String> originList = movieService.getOriginList(movie.getPlayItem());

			System.out.println(urlMap.size());

			VideoDetail videoDetail = new VideoDetail();
			videoDetail.setOrigin(originList);
			videoDetail.setUrls(urlMap);
			videoDetail.setComment(movie.getComment());
			videoDetail.setTime(movie.getTime());
			videoDetail.setTitle(movie.getTitle());
			videoDetail.setDirector(movie.getDirector());
			videoDetail.setImgUrl(movie.getImgUrl());
			videoDetail.setRegion(movie.getRegion());
			videoDetail.setStarts(movie.getStars());
			videoDetail.setTotalNumber("1");
			videoDetail.setType(movie.getType());

			// 将数据放到request
			Map<String, Object> request = ActionContext.getContext().getContextMap();
			request.put("videoDetail", videoDetail);
			return "item";
		} else {
			return "index";
		}
	}

	/**
	 * 选择播放器
	 * 
	 * @return
	 */
	public String selectPlayer() {
		// 将播放地址放在request域中
		// 将数据放到request
		Map<String, Object> request = ActionContext.getContext().getContextMap();
		request.put("url", url);
		return "select";
	}
	
	/**
	 * 下一页
	 * 
	 * @return
	 */
	public String indexNext() {
		// 查询第p页
		List<Movie> movieList = movieService.getVideoList(page * 30, 30);
		// 将数据放到request
		Map<String, Object> request = ActionContext.getContext().getContextMap();
		request.put("movieList", movieList);
		request.put("page", page);
		//页码放到session
		return "index";

	}

}
