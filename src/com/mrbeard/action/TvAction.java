package com.mrbeard.action;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.mrbeard.entity.Tv;
import com.mrbeard.entity.Tv_PlayItem;
import com.mrbeard.entity.VideoDetail;
import com.mrbeard.entity.VideoProperty;
import com.mrbeard.service.TvService;
import com.mrbeard.util.VideoUtil;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

@Component
public class TvAction extends ActionSupport {

	@Resource
	private TvService tvService;

	/**
	 * 页码
	 */
	private int page;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	// 标题
	private String title;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * 接受视频参数
	 */
	private VideoProperty videoProperty;

	public void setVideoProperty(VideoProperty videoProperty) {
		this.videoProperty = videoProperty;
	}

	public VideoProperty getVideoProperty() {
		return videoProperty;
	}

	/**
	 * 访问主页
	 * 
	 * @return
	 */
	public String index() {

		// 查询前30条
		List<Tv> tvList = tvService.getVideoList(0, 30);
		// 将数据放到request
		Map<String, Object> request = ActionContext.getContext().getContextMap();
		request.put("tvList", tvList);
		request.put("page", 0);
		return "index";

	}

	/**
	 * 查询并到结果页
	 * 
	 * @return
	 */
	public String item() {
		// 查询
		if (!title.isEmpty()) {
			List<Tv> result = tvService.getVideo(title);
			Tv tv = result.get(0);// 获取到电视剧
			System.out.println(tv.getTitle());
			// 获取到playItemList

			Map<Integer, List<String>> urlMap = tvService.getPlayItem(tv.getPlayItem());

			List<String> originList = tvService.getOriginList(tv.getPlayItem());

			System.out.println(urlMap.size());

			VideoDetail videoDetail = new VideoDetail();
			videoDetail.setOrigin(originList);
			videoDetail.setUrls(urlMap);
			videoDetail.setComment(tv.getComment());
			videoDetail.setTime(tv.getTime());
			videoDetail.setTitle(tv.getTitle());
			videoDetail.setDirector(tv.getDirector());
			videoDetail.setImgUrl(tv.getImgUrl());
			videoDetail.setRegion(tv.getRegion());
			videoDetail.setStarts(tv.getStars());
			videoDetail.setTotalNumber(tv.getTotalNumber());
			videoDetail.setType(tv.getType());

			// 将数据放到request
			Map<String, Object> request = ActionContext.getContext().getContextMap();
			request.put("videoDetail", videoDetail);
			return "item";
		} else {
			return "index";
		}

	}

	/**
	 * 选择播放器
	 * 
	 * @return
	 */
	public String selectPlayer() {
		// 将播放地址放在request域中
		// 将数据放到request
		Map<String, Object> request = ActionContext.getContext().getContextMap();
		request.put("url", url);
		return "select";
	}

	/**
	 * 下一页
	 * 
	 * @return
	 */
	public String indexNext() {
		// 查询第p页
		List<Tv> tvList = tvService.getVideoList(page * 30, 30);
		// 将数据放到request
		Map<String, Object> request = ActionContext.getContext().getContextMap();
		request.put("tvList", tvList);
		request.put("page", page);
		// 页码放到session
		return "index";

	}

	/**
	 * 根据属性模糊查找
	 * 
	 * @return
	 */
	public String getByPrames() {

		List<Tv> tvList = tvService.getVideoByType(videoProperty.getType(), videoProperty.getTime(),
				videoProperty.getRegion(), page * 30, 30);
		// 将数据放到request
		Map<String, Object> request = ActionContext.getContext().getContextMap();
		request.put("tvList", tvList);
		request.put("page", page);
		
		return "index";
	}

}
