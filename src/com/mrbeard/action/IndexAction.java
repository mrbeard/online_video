package com.mrbeard.action;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.mrbeard.entity.Animation;
import com.mrbeard.entity.Movie;
import com.mrbeard.entity.Tv;
import com.mrbeard.entity.Variety;
import com.mrbeard.service.AnimationService;
import com.mrbeard.service.MovieService;
import com.mrbeard.service.TvService;
import com.mrbeard.service.VarietyService;
import com.opensymphony.xwork2.ActionContext;

@Component
public class IndexAction {
	
	@Resource
	MovieService movieService;
	@Resource
	TvService tvService;
	@Resource
	AnimationService animationService;
	@Resource
	VarietyService varietyService;
	
	private String title;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	private String url;
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}


	/**
	 * 显示首页
	 * @return
	 */
	public String index() {
		//查询前12条数据
		List<Movie> movieList =  movieService.getVideoList(0, 12);
		List<Tv> tvList =  tvService.getVideoList(0, 12);
		List<Animation> animationList =  animationService.getVideoList(0, 12);
		List<Variety> varietyList =  varietyService.getVideoList(0, 12);
		
		
		// 将数据放到request
		Map<String, Object> request = ActionContext.getContext().getContextMap();
		request.put("movieList", movieList);
		request.put("tvList", tvList);
		request.put("animationList", animationList);
		request.put("varietyList", varietyList);
		
		return "index";
	}
	
	
	/**
	 * 选择播放器
	 * @return
	 */
	public String selectPlayer() {
		//将播放地址放在request域中
		//将数据放到request
		Map<String, Object> request =  ActionContext.getContext().getContextMap();
		request.put("url", url);
		return "select";
	}
	
	/**
	 * 搜索
	 * @return
	 */
	public String search() {
		//搜索电影
		List<Movie> movieList =  movieService.getVideo(title);
		//搜索电视剧
		List<Tv> tvList =  tvService.getVideo(title);
		//搜索动漫
		List<Animation> animationList =  animationService.getVideo(title);
		//搜索综艺
		List<Variety> varietyList =  varietyService.getVideo(title);
		
		//获取总结果条数
		int totalNumber = movieList.size()+tvList.size()+animationList.size()+varietyList.size();
		
		//将数据放到request
		Map<String, Object> request =  ActionContext.getContext().getContextMap();
		
		request.put("title", title);
		request.put("totalNumber", totalNumber);
		request.put("tvList", tvList);
		request.put("movieList", movieList);
		request.put("animationList", animationList);
		request.put("varietyList", varietyList);
		
		return "search";
	}
	

}
