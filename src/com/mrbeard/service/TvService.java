package com.mrbeard.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mrbeard.dao.TvDao;
import com.mrbeard.dao.interfaces.IVideoDao;
import com.mrbeard.dao.interfaces.IVideoItemDao;
import com.mrbeard.entity.Tv;
import com.mrbeard.entity.Tv_PlayItem;

@Component
@Transactional
public class TvService  {

	@Resource
	private TvDao tvDao;

	public Map<Integer, List<String>> getPlayItem(Tv_PlayItem playItem) {
		// TODO Auto-generated method stub
		return tvDao.getPlayItem(playItem);
	}

	public List<String> getOriginList(Tv_PlayItem playItem) {
		// TODO Auto-generated method stub
		return tvDao.getOriginList(playItem);
	}

	public int getTotalNumber() {
		// TODO Auto-generated method stub
		return tvDao.getTotalNumber();
	}

	public List<Tv> getVideoList(int currentPage, int number) {
		// TODO Auto-generated method stub
		return tvDao.getVideoList(currentPage, number);
	}

	public Tv getVideo(Serializable id) {
		// TODO Auto-generated method stub
		return tvDao.getVideo(id);
	}

	public List<Tv> getVideo(String title) {
		// TODO Auto-generated method stub
		return tvDao.getVideo(title);
	}

	public List<Tv> getVideoByType(String type, String time, String region, int currentPage, int number) {
		// TODO Auto-generated method stub
		return tvDao.getVideoByType(type, time, region, currentPage, number);
	}


	

	
}
