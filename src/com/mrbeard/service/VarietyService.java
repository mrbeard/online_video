package com.mrbeard.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mrbeard.dao.TvDao;
import com.mrbeard.dao.VarietyDao;
import com.mrbeard.dao.interfaces.IVideoDao;
import com.mrbeard.dao.interfaces.IVideoItemDao;
import com.mrbeard.entity.Tv;
import com.mrbeard.entity.Tv_PlayItem;
import com.mrbeard.entity.Variety;
import com.mrbeard.entity.Variety_PlayItem;

@Component
@Transactional
public class VarietyService{

	@Resource
	private VarietyDao varietyDao;

	public Map<Integer, List<String>> getPlayItem(Variety_PlayItem playItem) {
		// TODO Auto-generated method stub
		return varietyDao.getPlayItem(playItem);
	}

	public List<String> getOriginList(Variety_PlayItem playItem) {
		// TODO Auto-generated method stub
		return varietyDao.getOriginList(playItem);
	}

	public int getTotalNumber() {
		// TODO Auto-generated method stub
		return varietyDao.getTotalNumber();
	}

	public List<Variety> getVideoList(int currentPage, int number) {
		// TODO Auto-generated method stub
		return varietyDao.getVideoList(currentPage, number);
	}

	public Variety getVideo(Serializable id) {
		// TODO Auto-generated method stub
		return varietyDao.getVideo(id);
	}

	public List<Variety> getVideo(String title) {
		// TODO Auto-generated method stub
		return varietyDao.getVideo(title);
	}

	public List<Variety> getVideoByType(String type, String time, String region, int currentPage, int number) {
		// TODO Auto-generated method stub
		return varietyDao.getVideoByType(type, time, region, currentPage, number);
	}


	

	
}
