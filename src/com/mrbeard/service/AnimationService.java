package com.mrbeard.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mrbeard.dao.AnimationDao;
import com.mrbeard.dao.TvDao;
import com.mrbeard.dao.interfaces.IVideoDao;
import com.mrbeard.dao.interfaces.IVideoItemDao;
import com.mrbeard.entity.Animation;
import com.mrbeard.entity.Animation_PlayItem;
import com.mrbeard.entity.Tv;
import com.mrbeard.entity.Tv_PlayItem;

@Component
@Transactional
public class AnimationService {

	@Resource
	private AnimationDao animationDao;

	public Map<Integer, List<String>> getPlayItem(Animation_PlayItem playItem) {
		// TODO Auto-generated method stub
		return animationDao.getPlayItem(playItem);
	}

	public List<String> getOriginList(Animation_PlayItem playItem) {
		// TODO Auto-generated method stub
		return animationDao.getOriginList(playItem);
	}

	public int getTotalNumber() {
		// TODO Auto-generated method stub
		return animationDao.getTotalNumber();
	}

	public List<Animation> getVideoList(int currentPage, int number) {
		// TODO Auto-generated method stub
		return animationDao.getVideoList(currentPage, number);
	}

	public Animation getVideo(Serializable id) {
		// TODO Auto-generated method stub
		return animationDao.getVideo(id);
	}

	public List<Animation> getVideo(String title) {
		// TODO Auto-generated method stub
		return animationDao.getVideo(title);
	}

	public List<Animation> getVideoByType(String type, String time, String region, int currentPage, int number) {
		// TODO Auto-generated method stub
		return animationDao.getVideoByType(type, time, region, currentPage, number);
	}


	

	
}
