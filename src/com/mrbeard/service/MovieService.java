package com.mrbeard.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mrbeard.dao.MovieDao;
import com.mrbeard.dao.interfaces.IVideoDao;
import com.mrbeard.dao.interfaces.IVideoItemDao;
import com.mrbeard.entity.Movie;
import com.mrbeard.entity.Movie_PlayItem;

@Transactional
@Component
public class MovieService   {
	
	@Resource
	private MovieDao movieDao;

	public Map<Integer, List<String>> getPlayItem(Movie_PlayItem playItem) {
		// TODO Auto-generated method stub
		return movieDao.getPlayItem(playItem);
	}

	public List<String> getOriginList(Movie_PlayItem playItem) {
		// TODO Auto-generated method stub
		return movieDao.getOriginList(playItem);
	}

	public int getTotalNumber() {
		// TODO Auto-generated method stub
		return movieDao.getTotalNumber();
	}

	public List<Movie> getVideoList(int currentPage, int number) {
		// TODO Auto-generated method stub
		return movieDao.getVideoList(currentPage, number);
	}

	public Movie getVideo(Serializable id) {
		// TODO Auto-generated method stub
		return movieDao.getVideo(id);
	}

	public List<Movie> getVideo(String title) {
		// TODO Auto-generated method stub
		return movieDao.getVideo(title);
	}

	public List<Movie> getVideoByType(String type, String time, String region, int currentPage, int number) {
		// TODO Auto-generated method stub
		return movieDao.getVideoByType(type, time, region, currentPage, number);
	}

}
