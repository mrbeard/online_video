# 在线影音

#### 项目介绍
Struts2+Spring+Hibernate项目，实现网络视频获取及解析观看。

#### 软件架构
软件采用MVC架构


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明
首页截图：
![![输入图片说明](https://images.gitee.com/uploads/images/2018/0723/173152_bbef0543_1799100.png "屏幕截图.png")](https://images.gitee.com/uploads/images/2018/0723/173144_4f4c5480_1799100.png "屏幕截图.png")

电影栏目
![输入图片说明](https://images.gitee.com/uploads/images/2018/0723/173227_1efc1649_1799100.png "屏幕截图.png")

搜索结果页
![输入图片说明](https://images.gitee.com/uploads/images/2018/0723/173323_5fbb5f0e_1799100.png "屏幕截图.png")

视频详情
![输入图片说明](https://images.gitee.com/uploads/images/2018/0723/173404_e3e1555d_1799100.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0723/173434_fa4e71db_1799100.png "屏幕截图.png")

选择播放器
![输入图片说明](https://images.gitee.com/uploads/images/2018/0723/173508_31a321b5_1799100.png "屏幕截图.png")

播放页
![输入图片说明](https://images.gitee.com/uploads/images/2018/0723/173632_dbfb3935_1799100.png "屏幕截图.png")
#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)