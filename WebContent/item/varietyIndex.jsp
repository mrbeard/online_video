<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>	
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>最新更新综艺在线观看_第${page }页 - 彬彬影视</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta http-equiv=X-UA-Compatible content="IE=edge,chrome=1">
<meta name="description" content="最新更新综艺下载_第${page }页 - 彬彬影视">
<link rel="miphtml" href="#">
<meta http-equiv="X-UA-COMPATIBLE" content="IE=edge">
<link rel="shortcut icon" href="${pageContext.request.contextPath }/images/favicon.png">
<link rel="apple-touch-icon-precomposed"
	href="${pageContext.request.contextPath }/images/logo/180.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="${pageContext.request.contextPath }/images/logo/72.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="${pageContext.request.contextPath }/images/logo/114.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="${pageContext.request.contextPath }/images/logo/144.png">
<link rel="apple-touch-icon-precomposed" sizes="180x180"
	href="${pageContext.request.contextPath }/images/logo/180.png">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath }/css/css.tv.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath }/css/bootstrap.min.css" />
</head>
<body>
	<header class="bc66dda2165be732cc2" id="bc66dda2165be732cc2">
		<a class="bdc25adbc0b2" ><img
			src="${pageContext.request.contextPath }/images/logo.svg"></a>
	</header>
	<div class="b666ebb7385af6efed4" id="b666ebb7385af6efed4">
		<div class="b502b40e" id="b502b40e">
			<span></span><span></span><span></span>
		</div>
	</div>
	<div class="b3761487a9bb">
		<a ><i class="b767 czs-home-l"></i></a>
	</div>
	<div class="b857684c" id="b857684c">
        <a class="b25ab" href="${pageContext.request.contextPath }/index_index.action">
            <img src="${pageContext.request.contextPath }/images/logo.svg" alt="b25ab">
        </a>
        <h1>彬彬影视</h1>
        <div class="bc3c84db07b73b705">MRBEARD.CLUB</div>
        <div class="b965ecadb b976c881daa ">
            <a href="${pageContext.request.contextPath }/index_index.action">
                <i class="b1ad czs-home-l"></i>
                <span>首页</span>
            </a>
        </div>
        <div class="b965ecadb b976c881daa ">
            <a href="${pageContext.request.contextPath }/movie_index.action">
                <i class="b1ad czs-film-l"></i>
                <span>电影</span>
            </a>
        </div>
        <div class="b965ecadb b4679a36c82 ">
            <a href="${pageContext.request.contextPath }/tv_index.action">
                <i class="b1ad czs-board-l"></i>
                <span>电视剧</span>
            </a>
        </div>
        <div class="b965ecadb b265cd7812f ">
            <a href="${pageContext.request.contextPath }/animation_index.action">
                <i class="b1ad czs-bar-chart-l"></i>
                <span>动漫</span>
            </a>
        </div>
        <div class="b965ecadb b265cd7812f ba621d6">
            <a href="${pageContext.request.contextPath }/variety_index.action">
                <i class="b1ad czs-bookmark-l"></i>
                <span>综艺</span>
            </a>
        </div>
        <a class="be9fb15597" href="${pageContext.request.contextPath }/index_index.action">&#169; MRBEARD.CLUB</a>
    </div>
	<div class="bc1824a0fc">
		<div class="b3fcd">
			<div class="ba9078 b7bee4b322729">
				<div class="b52b80ca9">
					<i class="czs-circle"></i><span class="ba000236b4a47624">综艺</span><span
						class="b329592b4fa47d94">共收录<em> 12155 </em>部
					</span>
				</div>
			</div>
			<ul class="be431130ded">
				<li><span class="b25ea6177d3 b152001a052">类型：</span><a
					href="#" class="bf43252f2">全部</a><a
					href="#">剧情</a><a href="/tv/t/3/sort/new.html">喜剧</a><a
					href="/tv/t/2/sort/new.html">爱情</a><a href="/tv/t/5/sort/new.html">动作</a><a
					href="/tv/t/31/sort/new.html">武侠</a><a
					href="/tv/t/21/sort/new.html">动画</a><a
					href="/tv/t/20/sort/new.html">短片</a><a
					href="/tv/t/28/sort/new.html">惊悚</a><a
					href="/tv/t/27/sort/new.html">纪录片</a><a
					href="/tv/t/7/sort/new.html">犯罪</a><a href="/tv/t/6/sort/new.html">悬疑</a><a
					href="/tv/t/29/sort/new.html">冒险</a><a
					href="/tv/t/22/sort/new.html">奇幻</a><a
					href="/tv/t/4/sort/new.html">科幻</a><a
					href="/tv/t/19/sort/new.html">家庭</a><a
					href="/tv/t/8/sort/new.html">恐怖</a><a
					href="/tv/t/11/sort/new.html">战争</a><a
					href="/tv/t/18/sort/new.html">音乐</a><a
					href="/tv/t/32/sort/new.html">历史</a><a
					href="/tv/t/33/sort/new.html">同性</a></li>
				<li><span class="b25ea6177d3 b152001a052">年份：</span><a
					href="/tv/y/0/sort/new.html" class="bf43252f2">全部</a><a
					href="/tv/y/2018/sort/new.html">2018</a><a
					href="/tv/y/2017/sort/new.html">2017</a><a
					href="/tv/y/2016/sort/new.html">2016</a><a
					href="/tv/y/2015/sort/new.html">2015</a><a
					href="/tv/y/2014/sort/new.html">2014</a><a
					href="/tv/y/2013/sort/new.html">2013</a><a
					href="/tv/y/2012/sort/new.html">2012</a><a
					href="/tv/y/2011/sort/new.html">2011</a><a
					href="/tv/y/2010/sort/new.html">2010</a><a
					href="/tv/y/2009/sort/new.html">2009</a><a
					href="/tv/y/2008/sort/new.html">2008</a><a
					href="/tv/y/10/sort/new.html">十年前</a></li>
				<li><span class="b25ea6177d3 b152001a052">地区：</span><a
					href="/tv/c/0/sort/new.html" class="bf43252f2">全部</a><a
					href="/tv/c/4/sort/new.html">中国大陆</a><a
					href="/tv/c/5/sort/new.html">香港</a><a href="/tv/c/1/sort/new.html">美国</a><a
					href="/tv/c/3/sort/new.html">日本</a><a href="/tv/c/2/sort/new.html">英国</a><a
					href="/tv/c/16/sort/new.html">韩国</a><a
					href="/tv/c/10/sort/new.html">法国</a><a
					href="/tv/c/20/sort/new.html">德国</a><a
					href="/tv/c/8/sort/new.html">加拿大</a><a
					href="/tv/c/15/sort/new.html">台湾</a></li>
			</ul>
			<div class="bdc3a7 b113cba5f3d08">
				<div class="bde99b40a3b">
					<div class="b49c b94536551">
						<c:forEach var="va" items="${varietyList }" varStatus="vs">
						
							<div class="b0755 bd42c bf496 be07f">
								<div class="b743d">
									<a class="b0feffa7307e5" title="${va.title }"
										href="${pageContext.request.contextPath }/variety_item.action?title=${va.title }"><div class="bbc631">
											<img
												src="${va.imgUrl }"
												style="display: block;"
												onerror="this.onerror='';src='${pageContext.request.contextPath }/images/error.gif'">
												<span class="b23a2">${va.time }</span><i></i>
										</div>
										<span class="b7ddad1f368">${va.title }</span></a>
									<div class="b736cbf8ee">
										<span class='span-year span-year-2018'>${va.time }</span> / ${va.type }
									</div>
								</div>
							</div>
						</c:forEach>
						
					</div>
				</div>
				<div class="bde99b40a3b">
					<div class="b49c b94536551">
						<center>
						<input type="hidden" id="currentPage" value="${page }">
						<button type="button" class="btn btn-success">
							<a style="text-decoration:none; 
								out-line: none; color: #FFFFFF;"
								onclick="previousPage()">
								上一页
							</a>
						</button>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<button type="button" class="btn btn-success">
							<a style="text-decoration:none; 
								out-line: none; color: #FFFFFF;"
								onclick="nextPage()">
								下一页
							</a>
						</button>
					</center>
					</div>
				</div>
				
			</div>
			<footer class="b2ddfe3">
				<div class="bcc0e432cc8c5ef375"></div>
				Copyright &#169; 2016-2018 彬彬影视 <a
					href="http://www.miitbeian.gov.cn/" rel="nofollow"
					style="color: #bbb; text-decoration: none;" target="_blank"
					rel="nofollow">琼ICP备16003139号-3</a>&nbsp;&nbsp;&nbsp;邮箱：hbz#foxmail.com
			</footer>
		</div>
	</div>
	<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath }/js/js.jss"></script>
	<script data-no-instant>InstantClick.init();</script>
	

	<script type="text/javascript">
	
		function previousPage(){
			if($("#currentPage").val() == 0){
				alert("没有上一页了");
			}else{
				var numb = 1;
				var page = $("#currentPage").val();
				numb = Number(page) -numb;
				window.location.href="/myssh/variety_indexNext.action?page="+numb; 
			}
		}
		
		function nextPage(){
			if($("#currentPage").val() == 30){
				alert("没有下一页了");
			}else{
				var numb = 1;
				var page = $("#currentPage").val();
				numb = numb + Number(page);
				window.location.href="/myssh/variety_indexNext.action?page="+numb; 
			}
		}
	</script>
	<script type="text/javascript">
		$(function() {
			 if(location.href.indexOf('#reloaded')==-1){
				 location.href=location.href+"#reloaded";
				 location.reload();
			}
		})
	</script>
</body>
</html>

