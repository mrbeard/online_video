<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>${title }的搜索结果</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv=X-UA-Compatible content="IE=edge,chrome=1">
    <meta name="keywords" content="${title }的搜索结果">
    <meta name="description" content="${title }的搜索结果">
    <link rel="miphtml" href="https://mip.btbuluo.com/s/%E4%BD%A0%E5%A5%BD.html">
    <meta http-equiv="X-UA-COMPATIBLE" content="IE=edge">
    <link rel="shortcut icon" href="${pageContext.request.contextPath }/images/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath }/images/logo/180.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="${pageContext.request.contextPath }/images/logo/72.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="${pageContext.request.contextPath }/images/logo/114.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="${pageContext.request.contextPath }/images/logo/144.png">
    <link rel="apple-touch-icon-precomposed" sizes="180x180" href="${pageContext.request.contextPath }/images/logo/180.png">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/css.search.css" />
</head>

<body>
    <header class="bc66dda2165be732cc2" id="bc66dda2165be732cc2">
        <a class="bdc25adbc0b2" >
            <img src="${pageContext.request.contextPath }/images/logo.svg">
        </a>
    </header>
    <div class="b666ebb7385af6efed4" id="b666ebb7385af6efed4">
        <div class="b502b40e" id="b502b40e">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <div class="b3761487a9bb">
        <a>
            <i class="b767 czs-home-l"></i>
        </a>
    </div>
    <div class="b857684c" id="b857684c">
        <a class="b25ab" href="${pageContext.request.contextPath }/index_index.action">
            <img src="${pageContext.request.contextPath }/images/logo.svg" alt="b25ab">
        </a>
        <h1>彬彬影视</h1>
        <div class="bc3c84db07b73b705">MRBEARD.CLUB</div>
        <div class="b965ecadb b976c881daa ">
            <a href="${pageContext.request.contextPath }/index_index.action">
                <i class="b1ad czs-home-l"></i>
                <span>首页</span>
            </a>
        </div>
        <div class="b965ecadb b976c881daa ">
            <a href="${pageContext.request.contextPath }/movie_index.action">
                <i class="b1ad czs-film-l"></i>
                <span>电影</span>
            </a>
        </div>
        <div class="b965ecadb b4679a36c82 ">
            <a href="${pageContext.request.contextPath }/tv_index.action">
                <i class="b1ad czs-board-l"></i>
                <span>电视剧</span>
            </a>
        </div>
        <div class="b965ecadb b265cd7812f ">
            <a href="${pageContext.request.contextPath }/animation_index.action">
                <i class="b1ad czs-bar-chart-l"></i>
                <span>动漫</span>
            </a>
        </div>
        <div class="b965ecadb b265cd7812f ba621d6">
            <a href="${pageContext.request.contextPath }/variety_index.action">
                <i class="b1ad czs-bookmark-l"></i>
                <span>综艺</span>
            </a>
        </div>
        <a class="be9fb15597" href="${pageContext.request.contextPath }/index_index.action">&#169; MRBEARD.CLUB</a>
    </div>
    <div class="bc1824a0fc">
        <div class="b3fcd">
            <div class="ba9078 b7bee4b322729">
                <div class="b52b80ca9">
                    <i class="czs-circle"></i>
                    <span class="ba000236b4a47624"></span>
                    <span class="b329592b4fa47d94">
                        找到
                        <em>${totalNumber }</em> 条符合搜索条件 "
                        <strong>${title }</strong>" 的结果.


                    </span>
                </div>
                <div class="b8086ec657 b184d712cf">
                    <form method="get" id="list-search" style="text-align: center;float: none" action="/myssh/index_search.action" >
                        <input tabindex="2" class="b6eea872d" id="searchname" name="title" type="bb1fe" placeholder="电影名 / 演员名 / 导演名"
                            value="${title }">
                        <input class="b874098437" type="submit" id="btn-search" value="搜索">
                    </form>
                </div>
            </div>
            <div class="ba2f0d92 b113cba5f3d08">
                <div class="b94536551">
                    
                    <c:forEach var="tv" items="${tvList }" varStatus="vs">
	                    <div class="b0f2eb16828 bdc56aa10af6">
	                        <div class="b49c">
	                            <div class="b775b b8616 b8428 bc937524">
	                                <a href="${pageContext.request.contextPath }/tv_item.action?title=${tv.title }">
	                                    <img src="${tv.imgUrl }" style="display: block;"
	                                    onerror="this.onerror='';src='${pageContext.request.contextPath }/images/error.gif'">
	                                </a>
	                            </div>
	                            <div class="b6f931 bb82a b4d97 bf9e438">
	                                <h2 class="b8e964be">
	                                    <a  class="b8b33ab51b4">${tv.title }</a>
	                                    <i>(${tv.time })</i>
	                                </h2>
	                                <p class="">
	                                    <span class="b74be3c">地区：</span>
	                                    <a class="ba9c94a8161 b82b185">${tv.region }</a>&nbsp; </p>
	                                <p class="">
	                                    <span class="b74be3c">类型：</span>
	                                    <a  class="ba9c94a8161 b82b185">${tv.type }</a>&nbsp;
	                                <p class="b184d712cf">
	                                    <span class="b74be3c">导演：</span>
	                                    <a  class="ba9c94a8161 b82b185">${tv.director }</a>&nbsp; </p>
	                                <p>
	                                    <span class="b74be3c">主演：</span>
	                                    <a  class="ba9c94a8161 b82b185">${tv.stars }</a>&nbsp;
	                                <p class="b184d712cf">
	                                    <span class="b74be3c">简介：</span>
	                                    <span class="ba9c94a8161 b0daf6cd48e318e8">${tv.comment }</span>
	                                </p>
	                                <p class="b184d712cf">
	                                    <a  href="${pageContext.request.contextPath }/tv_item.action?title=${tv.title }" class="b978556 b01953">查看详情</a>
	                                </p>
	                            </div>
	                        </div>
	                    </div>
                    </c:forEach>
                    
                    
                    <c:forEach var="mo" items="${movieList }" varStatus="vs">
	                    <div class="b0f2eb16828 bdc56aa10af6">
	                        <div class="b49c">
	                            <div class="b775b b8616 b8428 bc937524">
	                                <a href="${pageContext.request.contextPath }/movie_item.action?title=${mo.title }">
	                                    <img src="${mo.imgUrl }" style="display: block;"
	                                    onerror="this.onerror='';src='${pageContext.request.contextPath }/images/error.gif'">
	                                </a>
	                            </div>
	                            <div class="b6f931 bb82a b4d97 bf9e438">
	                                <h2 class="b8e964be">
	                                    <a  class="b8b33ab51b4">${mo.title }</a>
	                                    <i>(${mo.time })</i>
	                                </h2>
	                                <p class="">
	                                    <span class="b74be3c">地区：</span>
	                                    <a class="ba9c94a8161 b82b185">${mo.region }</a>&nbsp; </p>
	                                <p class="">
	                                    <span class="b74be3c">类型：</span>
	                                    <a  class="ba9c94a8161 b82b185">${mo.type }</a>&nbsp;
	                                <p class="b184d712cf">
	                                    <span class="b74be3c">导演：</span>
	                                    <a  class="ba9c94a8161 b82b185">${mo.director }</a>&nbsp; </p>
	                                <p>
	                                    <span class="b74be3c">主演：</span>
	                                    <a  class="ba9c94a8161 b82b185">${mo.stars }</a>&nbsp;
	                                <p class="b184d712cf">
	                                    <span class="b74be3c">简介：</span>
	                                    <span class="ba9c94a8161 b0daf6cd48e318e8">${mo.comment }</span>
	                                </p>
	                                <p class="b184d712cf">
	                                    <a  href="${pageContext.request.contextPath }/tv_item.action?title=${mo.title }" class="b978556 b01953">查看详情</a>
	                                </p>
	                            </div>
	                        </div>
	                    </div>
                    </c:forEach>
                    
                    <c:forEach var="an" items="${animationList }" varStatus="vs">
	                    <div class="b0f2eb16828 bdc56aa10af6">
	                        <div class="b49c">
	                            <div class="b775b b8616 b8428 bc937524">
	                                <a href="${pageContext.request.contextPath }/animation_item.action?title=${an.title }">
	                                    <img src="${an.imgUrl }" style="display: block;"
	                                    onerror="this.onerror='';src='${pageContext.request.contextPath }/images/error.gif'">
	                                </a>
	                            </div>
	                            <div class="b6f931 bb82a b4d97 bf9e438">
	                                <h2 class="b8e964be">
	                                    <a  class="b8b33ab51b4">${an.title }</a>
	                                    <i>(${an.time })</i>
	                                </h2>
	                                <p class="">
	                                    <span class="b74be3c">地区：</span>
	                                    <a class="ba9c94a8161 b82b185">${an.region }</a>&nbsp; </p>
	                                <p class="">
	                                    <span class="b74be3c">类型：</span>
	                                    <a  class="ba9c94a8161 b82b185">${an.type }</a>&nbsp;
	                                <p class="b184d712cf">
	                                    <span class="b74be3c">导演：</span>
	                                    <a  class="ba9c94a8161 b82b185">${an.director }</a>&nbsp; </p>
	                                <p>
	                                    <span class="b74be3c">主演：</span>
	                                    <a  class="ba9c94a8161 b82b185">${an.stars }</a>&nbsp;
	                                <p class="b184d712cf">
	                                    <span class="b74be3c">简介：</span>
	                                    <span class="ba9c94a8161 b0daf6cd48e318e8">${an.comment }</span>
	                                </p>
	                                <p class="b184d712cf">
	                                    <a  href="${pageContext.request.contextPath }/animation_item.action?title=${an.title }" class="b978556 b01953">查看详情</a>
	                                </p>
	                            </div>
	                        </div>
	                    </div>
                    </c:forEach>
                    
                    <c:forEach var="va" items="${varietyList }" varStatus="vs">
	                    <div class="b0f2eb16828 bdc56aa10af6">
	                        <div class="b49c">
	                            <div class="b775b b8616 b8428 bc937524">
	                                <a href="${pageContext.request.contextPath }/variety_item.action?title=${va.title }">
	                                    <img src="${va.imgUrl }" style="display: block;"
	                                    onerror="this.onerror='';src='${pageContext.request.contextPath }/images/error.gif'">
	                                </a>
	                            </div>
	                            <div class="b6f931 bb82a b4d97 bf9e438">
	                                <h2 class="b8e964be">
	                                    <a  class="b8b33ab51b4">${va.title }</a>
	                                    <i>(${va.time })</i>
	                                </h2>
	                                <p class="">
	                                    <span class="b74be3c">地区：</span>
	                                    <a class="ba9c94a8161 b82b185">${va.region }</a>&nbsp; </p>
	                                <p class="">
	                                    <span class="b74be3c">类型：</span>
	                                    <a  class="ba9c94a8161 b82b185">${va.type }</a>&nbsp;
	                                <p class="b184d712cf">
	                                    <span class="b74be3c">导演：</span>
	                                    <a  class="ba9c94a8161 b82b185">${va.director }</a>&nbsp; </p>
	                                <p>
	                                    <span class="b74be3c">主演：</span>
	                                    <a  class="ba9c94a8161 b82b185">${va.stars }</a>&nbsp;
	                                <p class="b184d712cf">
	                                    <span class="b74be3c">简介：</span>
	                                    <span class="ba9c94a8161 b0daf6cd48e318e8">${va.comment }</span>
	                                </p>
	                                <p class="b184d712cf">
	                                    <a  href="${pageContext.request.contextPath }/variety_item.action?title=${va.title }" class="b978556 b01953">查看详情</a>
	                                </p>
	                            </div>
	                        </div>
	                    </div>
                    </c:forEach>
                    
                </div>
            </div>
            <div id="page" class="b1563">
                <ul class="pagination">
                    <li class="disabled">
                        <span>&laquo;</span>
                    </li>
                    <li class="ba621d6">
                        <span>1</span>
                    </li>
                    <li>
                        <a href="/s/%E4%BD%A0%E5%A5%BD.html?p=2">2</a>
                    </li>
                    <li>
                        <a href="/s/%E4%BD%A0%E5%A5%BD.html?p=3">3</a>
                    </li>
                    <li>
                        <a href="/s/%E4%BD%A0%E5%A5%BD.html?p=2">&raquo;</a>
                    </li>
                </ul>
            </div>
            <script>
                var page = 1;
            </script>
            <footer class="b2ddfe3">
                <div class="bcc0e432cc8c5ef375"></div> Copyright &#169; 2016-2018 彬彬影视
                <a href="http://www.miitbeian.gov.cn/" rel="nofollow" style="color:#bbb;text-decoration: none;"
                    target="_blank" rel="nofollow">琼ICP备16003139号-3</a>&nbsp;&nbsp;&nbsp;邮箱：hbz#foxmail.com </footer>
        </div>
    </div>
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath }/js/sjs.jss"></script>
    <script data-no-instant>
        InstantClick.init();
    </script>
    <script data-no-instant>
        var _hmt = _hmt || [];
        (function () {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?cff4119c82a206302d3f47df2c74d907";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
    <script>
        (function () {
            var c = document.createElement("script");
            var b = window.location.protocol.split(":")[0];
            if (b === "https") {
                c.src = "https://zz.bdstatic.com/linksubmit/push.js"
            } else {
                c.src = "http://push.zhanzhang.baidu.com/push.js"
            }
            var a = document.getElementsByTagName("script")[0];
            a.parentNode.insertBefore(c, a)
        })();
    </script>
    
    <script type="text/javascript">
    	$("#btn-search").click(function(){
    		if($("#searchname").val() == ""){
    			alert("搜索内容为空！请输入内容再试！");
    		}else{
    			window.location.href="/myssh/index_search.action?title="+$("#searchname").val();
    		}
    	});
    </script>
</body>

</html>