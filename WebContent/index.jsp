<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>彬彬影视 - 注重体验与质量的影视资源下载网站</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta http-equiv=X-UA-Compatible content="IE=edge,chrome=1">
<meta name="keywords"
	content="彬彬影视,BT电影下载,BT电影天堂,BT天堂,飘花电影院,电影迅雷下载,高清电影下载">
<meta name="description"
	content="彬彬影视是一个注重体验与质量的影视资源下载网站，每天更新720p、1080p，蓝光高清等电影种子资源">
<link rel="miphtml" href="https://mip.btbuluo.com/">
<meta http-equiv="X-UA-COMPATIBLE" content="IE=edge">
<link rel="shortcut icon" href="${pageContext.request.contextPath }/images/favicon.png">
<link rel="apple-touch-icon-precomposed"
	href="${pageContext.request.contextPath }/images/logo/180.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="${pageContext.request.contextPath }/images/logo/72.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="${pageContext.request.contextPath }/images/logo/114.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="${pageContext.request.contextPath }/images/logo/144.png">
<link rel="apple-touch-icon-precomposed" sizes="180x180"
	href="${pageContext.request.contextPath }/images/logo/180.png">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath }/css/css.css" />
</head>
<body>
	<header class="b9356e6022c622c59d7" id="b9356e6022c622c59d7">
		<a class="b9cf37d5834a"><img
			src="${pageContext.request.contextPath }/images/logo.svg"></a>
	</header>
	<div class="b190efcd7c4b25a917e" id="b190efcd7c4b25a917e">
		<div class="b0d0e17b" id="b0d0e17b">
			<span></span><span></span><span></span>
		</div>
	</div>
	<div class="be51d536816d">
		<a><i class="b8d6 czs-home-l"></i></a>
	</div>
	<div class="b309e7b5" id="b309e7b5">
		<a class="b7002" href="${pageContext.request.contextPath }/index_index.action "><img src="${pageContext.request.contextPath }/images/logo.svg"
			alt="b7002"></a>
		<h1>彬彬影视</h1>
		<div class="b37937b5ce50ca51f">MRBEARD.CLUB</div>
		<div class="bed30954c b957acfb992 b90562b">
			<a href="${pageContext.request.contextPath }/index_index.action "><i class="b9f7 czs-home-l"></i><span>首页</span></a>
		</div>
		<div class="bed30954c b957acfb992 ">
			<a href="${pageContext.request.contextPath }/movie_index.action "><i class="b9f7 czs-film-l"></i><span>电影</span></a>
		</div>
		<div class="bed30954c b9f2bc6028c ">
			<a href="${pageContext.request.contextPath }/tv_index.action"><i class="b9f7 czs-board-l"></i><span>电视剧</span></a>
		</div>
		<div class="bed30954c b59fcae2ef9 ">
			<a href="${pageContext.request.contextPath }/animation_index.action"><i class="b9f7 czs-bar-chart-l"></i><span>动漫</span></a>
		</div>
		<div class="bed30954c b59fcae2ef9 ">
			<a href="${pageContext.request.contextPath }/variety_index.action"><i class="b9f7 czs-bookmark-l"></i><span>综艺</span></a>
		</div>
		<a class="b073406e3e" href="${pageContext.request.contextPath }/index_index.action">&#169; MRBEARD.CLUB</a>
	</div>
	<div class="b77be5171e">
		<div class="b869d">
			<div class="ba4b557db9f">
				<h3>注重体验与质量的影视资源在线观看网站</h3>
				<form method="get" id="soform" action="/myssh/index_search.action"
					style="text-align: center; float: none">
					<input tabindex="2" class="b5964eb7f" id="searchname" 
						name="title"
						type="bab81" placeholder="电影名 / 演员名 / 导演名" value=""><input
						class="ba1090c43f" type="submit" id="btn-search" value="搜索" />
				</form>
			</div>
			<!-- 电影 -->
			<h3 class="b2462cac">
				热门电影<span class="bf8688324c7"></span><span class="b99ec33278"><a
					href="/moive/t/5/y/0/c/0/sort/new.html" class="b5a8bd1 b1fa08">动作</a><span
					class="b1fa08 ba0c6f25c734e8">|</span><a
					href="/moive/t/3/y/0/c/0/sort/new.html" class="b5a8bd1 b1fa08">喜剧</a><span
					class="b1fa08 ba0c6f25c734e8">|</span><a
					href="/moive/t/1/y/0/c/0/sort/new.html" class="b5a8bd1 b1fa08">爱情</a><span
					class="b1fa08 ba0c6f25c734e8">|</span><a
					href="/moive/t/29/y/0/c/0/sort/good.html" class="b5a8bd1 b1fa08">冒险</a><span
					class="b1fa08 ba0c6f25c734e8">|</span><a
					href="/moive/t/4/y/0/c/0/sort/hot.html" class="b5a8bd1 b1fa08">科幻</a></span><a
					href="${pageContext.request.contextPath }/movie_index.action" class="bfc87b0">查看更多 <i class="b8d6 czs-add"></i></a>
			</h3>
			<div class="bd4679">
				<div class="ba4c0733447">
					<div class="b526">
					<c:forEach var="movie" items="${movieList }" varStatus="vs">
						
						<div class="b9c26 bdb87 b74f3 b0667">
							<div class="b31f3">
								<a class="b6442e3585552" title="${movie.title }"
									href="${pageContext.request.contextPath }/movie_item.action?title=${movie.title}">
									<div class="b8d849">
										<img
											src="${movie.imgUrl }"
											onerror="this.onerror='';src='${pageContext.request.contextPath }/images/error.gif'"
											style="display: block;"><span class="bd78a">${movie.time }</span><i></i>
									</div>
									<span class="ba9b57084e9">${movie.title }<em></em></span>
								</a>
								<div class="bf7880a073">
									<span class='span-year span-year-2018'>${movie.time }</span> / <span
										class='span-high'>高分</span> /${movie.type }
								</div>
							</div>
						</div>
					</c:forEach>	
						
					</div>
				</div>
			</div>
			<!-- 电视剧 -->
			<h3 class="b2462cac">
				热播电视剧<span class="bf8688324c7"></span><span class="b99ec33278"><a
					href="/tv/t/0/y/0/c/4/sort/hot.html" class="b5a8bd1 b1fa08"> 爱情
				</a><span class="b1fa08 ba0c6f25c734e8">|</span><a
					href="/tv/t/0/y/0/c/16/sort/hot.html" class="b5a8bd1 b1fa08">
						喜剧 </a><span class="b1fa08 ba0c6f25c734e8">|</span><a
					href="/tv/t/0/y/0/c/1/sort/hot.html" class="b5a8bd1 b1fa08"> 动作
				</a></span><a href="${pageContext.request.contextPath }/tv_index.action" class="bfc87b0">查看更多 <i class="b8d6 czs-add"></i></a>
			</h3>
			<div class="bd4679">
				<div class="ba4c0733447">
					<div class="b526">
						
						<c:forEach var="tv" items="${tvList }" varStatus="vs">
						<div class="b9c26 bdb87 b74f3 b0667">
							<div class="b31f3">
								<a class="b6442e3585552" title="${tv.title }" 
								href="${pageContext.request.contextPath }/tv_item.action?title=${tv.title }"><div
										class="b8d849">
										<img
											src="${tv.imgUrl }"
											onerror="this.onerror='';src='${pageContext.request.contextPath }/images/error.gif'"
											style="display: block;"><span class="bd78a">${tv.time }</span><i></i>
									</div>
									<span class="ba9b57084e9">${tv.title }</span></a>
								<div class="bf7880a073">
									<span class='span-year span-year-2018'>${tv.time }</span> /${tv.type }
								</div>
							</div>
						</div>
						</c:forEach>
					</div>
				</div>
			</div>
			<!-- 动漫 -->
			<h3 class="b2462cac">
				热门动漫<span class="bf8688324c7"></span><span class="b99ec33278"><a
					href="/tv/t/0/y/0/c/4/sort/hot.html" class="b5a8bd1 b1fa08"> 国产
				</a><span class="b1fa08 ba0c6f25c734e8">|</span><a
					href="/tv/t/0/y/0/c/16/sort/hot.html" class="b5a8bd1 b1fa08">
						日本 </a><span class="b1fa08 ba0c6f25c734e8">|</span><a
					href="/tv/t/0/y/0/c/1/sort/hot.html" class="b5a8bd1 b1fa08"> 欧美
				</a></span><a href="${pageContext.request.contextPath }/animation_index.action" class="bfc87b0">查看更多 <i class="b8d6 czs-add"></i></a>
			</h3>
			<div class="bd4679">
				<div class="ba4c0733447">
					<div class="b526">
						
						<c:forEach var="animation" items="${animationList }" varStatus="vs">
						<div class="b9c26 bdb87 b74f3 b0667">
							<div class="b31f3">
								<a class="b6442e3585552" title="${animation.title }" 
								href="${pageContext.request.contextPath }/animation_item.action?title=${animation.title }"><div
										class="b8d849">
										<img
											src="${animation.imgUrl }"
											onerror="this.onerror='';src='${pageContext.request.contextPath }/images/error.gif'"
											style="display: block;"><span class="bd78a">${animation.time }</span><i></i>
									</div>
									<span class="ba9b57084e9">${animation.title }</span></a>
								<div class="bf7880a073">
									<span class='span-year span-year-2018'>${animation.time }</span> /${animation.type } 
								</div>
							</div>
						</div>
						</c:forEach>

						
					</div>
				</div>
			</div>
			<!-- 综艺 -->
			<h3 class="b2462cac">
				热门综艺<span class="bf8688324c7"></span><span class="b99ec33278"><a
					href="/tv/t/0/y/0/c/4/sort/hot.html" class="b5a8bd1 b1fa08"> 情感
				</a><span class="b1fa08 ba0c6f25c734e8">|</span><a
					href="/tv/t/0/y/0/c/16/sort/hot.html" class="b5a8bd1 b1fa08">
						播报 </a><span class="b1fa08 ba0c6f25c734e8">|</span><a
					href="/tv/t/0/y/0/c/1/sort/hot.html" class="b5a8bd1 b1fa08"> 真人秀
				</a></span><a href="${pageContext.request.contextPath }/variety_index.action" class="bfc87b0">查看更多 <i class="b8d6 czs-add"></i></a>
			</h3>
			<div class="bd4679">
				<div class="ba4c0733447">
					<div class="b526">
						
						
						
						<c:forEach var="variety" items="${varietyList }" varStatus="vs">
						<div class="b9c26 bdb87 b74f3 b0667">
							<div class="b31f3">
								<a class="b6442e3585552" title="${variety.title }" 
								href="${pageContext.request.contextPath }/variety_item.action?title=${variety.title }"><div
										class="b8d849">
										<img
											src="${variety.imgUrl }"
											onerror="this.onerror='';src='${pageContext.request.contextPath }/images/error.gif'"
											style="display: block;"><span class="bd78a">${variety.time }</span><i></i>
									</div>
									<span class="ba9b57084e9">${variety.title }</span></a>
								<div class="bf7880a073">
									<span class='span-year span-year-2018'>${variety.time }</span> /${variety.type } 
								</div>
							</div>
						</div>
						</c:forEach>
						
						
						
					</div>
				</div>
			</div>
			
			
			
			<footer class="bb15ef1">
				<div class="b6bf36fec8caae9d08"></div>
				Copyright &#169; 2016-2018 彬彬影视 <a
					href="http://www.miitbeian.gov.cn/" rel="nofollow"
					style="color: #bbb; text-decoration: none;" target="_blank"
					rel="nofollow">琼ICP备16003139号-3</a>&nbsp;&nbsp;&nbsp;邮箱：hbz#foxmail.com&nbsp;&nbsp;&nbsp;&nbsp;<span
					class="bb3f7b54d32e">友情链接：<a
					href="http://www.wukongshipin.com" target="_blank"
					style="color: #bbb; text-decoration: none;">悟空视频</a>&nbsp;&nbsp;<a
					href="http://bt0.com" target="_blank"
					style="color: #bbb; text-decoration: none;">1080P电影</a></span>
			</footer>
		</div>
	</div>
	<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath }/js/js.js"></script>
	<script data-no-instant>InstantClick.init();</script>
	<script data-no-instant>
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?cff4119c82a206302d3f47df2c74d907";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(hm, s);})();
	</script>
	<script>
	(function(){
		var c=document.createElement("script");
		var b=window.location.protocol.split(":")[0];
		if(b==="https"){
			c.src="https://zz.bdstatic.com/linksubmit/push.js"
		}else{
			c.src="http://push.zhanzhang.baidu.com/push.js"
		}
		var a=document.getElementsByTagName("script")[0];
		a.parentNode.insertBefore(c,a)
		})();
	</script>
	<script type="text/javascript">
	$("#btn-search").click(function(){
		if($("#searchname").val() == ""){
			alert("搜索内容为空！请输入内容再试！");
		}
	});
	</script>
	<script type="text/javascript">
		$(function() {
			 if(location.href.indexOf('#reloaded')==-1){
				 location.href=location.href+"#reloaded";
				 location.reload();
			}
		})
	</script>
</body>
</html>

