<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%-- <%@ taglib uri="/struts-tags"  prefix="s"%> --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>成功页面</title>
</head>
<body>
    欢迎你，${session.loginInfo.userName }
	<table align="center" border="1">
		<tr>
			<th>编号</th>
			<th>姓名</th>
			<th>入学日期</th>
			<th>班主任</th>
		</tr>
		<c:forEach var="data" items="${request.studentData }" varStatus="vs">
			<tr>
				<td>
					${vs.count }
				</td>
				<td>
					${data.name }
				</td>
				<td>
					${data.classDate }
				</td>
				<td>
					${data.teacher.name }
				</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>