<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>${videoDetail.title } </title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta http-equiv=X-UA-Compatible content="IE=edge,chrome=1">
<meta name="keywords" content="${videoDetail.title }">
<meta name="description"
	content="${videoDetail.title }">
<link rel="miphtml" href="https://mip.btbuluo.com/a/2d32f8f037.html">
<meta http-equiv="X-UA-COMPATIBLE" content="IE=edge">
<link rel="shortcut icon" href="${pageContext.request.contextPath }/images/favicon.png">
<link rel="apple-touch-icon-precomposed"
	href="${pageContext.request.contextPath }/images/logo/180.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="${pageContext.request.contextPath }/images/logo/72.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="${pageContext.request.contextPath }/images/logo/114.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="${pageContext.request.contextPath }/images/logo/144.png">
<link rel="apple-touch-icon-precomposed" sizes="180x180"
	href="${pageContext.request.contextPath }/images/logo/180.png">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath }/css/css.css" />
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath }/js/js.js"></script>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath }/css/bootstrap.min.css" />
</head>
<body>
	<header class="b9356e6022c622c59d7" id="b9356e6022c622c59d7">
		<a class="b9cf37d5834a"><img
			src="${pageContext.request.contextPath }/images/logo.svg"></a>
	</header>
	<div class="b190efcd7c4b25a917e" id="b190efcd7c4b25a917e">
		<div class="b0d0e17b" id="b0d0e17b">
			<span></span><span></span><span></span>
		</div>
	</div>
	<div class="be51d536816d">
		<a ><i class="b8d6 czs-home-l"></i></a>
	</div>
	<div class="b309e7b5" id="b309e7b5">
		<a class="b7002" href="${pageContext.request.contextPath }/index_index.action"><img src="${pageContext.request.contextPath }/images/logo.svg"
			alt="b7002"></a>
		<h1>彬彬影视</h1>
		<div class="b37937b5ce50ca51f">MRBEARD.CLUB</div>
		<div class="bed30954c b957acfb992 b90562b">
			<a href="${pageContext.request.contextPath }/index_index.action"><i class="b9f7 czs-home-l"></i><span>首页</span></a>
		</div>
		<div class="bed30954c b957acfb992 ">
			<a href="${pageContext.request.contextPath }/movie_index.action"><i class="b9f7 czs-film-l"></i><span>电影</span></a>
		</div>
		<div class="bed30954c b9f2bc6028c ">
			<a href="${pageContext.request.contextPath }/tv_index.action"><i class="b9f7 czs-board-l"></i><span>电视剧</span></a>
		</div>
		<div class="bed30954c b59fcae2ef9 ">
			<a href="${pageContext.request.contextPath }/animation_index.action"><i class="b9f7 czs-bar-chart-l"></i><span>动漫</span></a>
		</div>
		<div class="bed30954c b59fcae2ef9 ">
			<a href="${pageContext.request.contextPath }/variety_index.action"><i class="b9f7 czs-bookmark-l"></i><span>综艺</span></a>
		</div>
		<a class="b073406e3e" href="${pageContext.request.contextPath }/index_index.action">&#169; MRBEARD.CLUB</a>
	</div>
	<div class="b77be5171e">
		<div class="b869d">
			<div class="b01f1b bf5f5d4ab13ad">
				<div class="be0ef6619">
					<i class="czs-circle"></i><span class="b3ba918488c8ffde">详情</span>
				</div>
			</div>
			<div class="b10953d3">
				
				
				
				<div class="b0452465a17">
					<div class="b526">
						<div class="bccb6 b60f0c b1b02e be6cb498">
							<img
								src="${videoDetail.imgUrl }"
								style="display: block;"
								onerror="this.onerror='';src='${pageContext.request.contextPath }/images/error.gif'"
								>
						</div>
						<div class="b69910 b60f0c b1b02e bb8acb5">
							<div class="b2462cac">
								<h1>
									${videoDetail.title } <i></i>
								</h1>
							</div>
							
							<p class="">
								<span class="b5a8bd1">地区：</span><a
									class="bf8688324c7 b01a6f4">${videoDetail.region }</a>&nbsp;
							</p>
							<p class="">
								<span class="b5a8bd1">类型：</span><a
									class="bf8688324c7 b01a6f4">${videoDetail.type }</a>&nbsp;
							</p>
							<p class="">
								<span class="b5a8bd1">上映：</span><span class="bf8688324c7">${videoDetail.time }
								</span>
							</p>
							
							<p>
								<span class="b5a8bd1">导演：</span><a 
									class="bf8688324c7 b01a6f4">${videoDetail.director }</a>
							</p>
							<p class="ba3bb1">
								<span class="b5a8bd1">主演：</span><a 
									class="bf8688324c7 b01a6f4 ">${videoDetail.starts }</a>
							</p>
							<p>
								<span class="b5a8bd1">总集数：</span><span class="bf8688324c7">${videoDetail.totalNumber }</span>
							</p>
							
						
							<h2 class="ba44a2">剧情简介· · · · · ·</h2>
							<p class="b9942626fb">${videoDetail.comment }</p>
						</div>
					</div>
				</div>
				
				
				<c:forEach var="urls" items="${videoDetail.urls }" varStatus="vs">
					
						<div class="b0452465a17 b660c">
							<h3 class="ba6c21">
								<i class="czs-category-l b5c58aae56b39"></i> 来源：${urls.key+1 }
							</h3>
							<div class="bf40f19f" data-no-instant>
								<p style="word-spacing:8px; line-height: 40px;" >
									<c:forEach var="urls2" varStatus="vs2" items="${urls.value }">
										<a href="${pageContext.request.contextPath }/index_selectPlayer?url=${urls2 }" class="bc5f2c">
											<button type="button" style="width: 50px;" class="btn btn-info">${vs2.index+1 }</button>
										</a>
									</c:forEach>
								</p>
							</div>
						</div>
					
				</c:forEach>
			</div>
			<footer class="bb15ef1">
				<div class="b6bf36fec8caae9d08"></div>
				Copyright &#169; 2016-2018 BT部落天堂 <a
					href="http://www.miitbeian.gov.cn/" rel="nofollow"
					style="color: #bbb; text-decoration: none;" target="_blank"
					rel="nofollow">琼ICP备16003139号-3</a>&nbsp;&nbsp;&nbsp;邮箱：hbz#foxmail.com
			</footer>
		</div>
	</div>
	<script type="text/javascript">
		$(function() {
			 if(location.href.indexOf('#reloaded')==-1){
				 location.href=location.href+"#reloaded";
				 location.reload();
			}
		})
	</script>
	<script data-no-instant>InstantClick.init();</script>
	<script data-no-instant>var _hmt = _hmt || [];(function() {var hm = document.createElement("script");hm.src = "https://hm.baidu.com/hm.js?cff4119c82a206302d3f47df2c74d907";var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(hm, s);})();</script>
	<script>(function(){var c=document.createElement("script");var b=window.location.protocol.split(":")[0];if(b==="https"){c.src="https://zz.bdstatic.com/linksubmit/push.js"}else{c.src="http://push.zhanzhang.baidu.com/push.js"}var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(c,a)})();</script>
</body>
</html>

