<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>选择播放器 - 彬彬影视</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta http-equiv=X-UA-Compatible content="IE=edge,chrome=1">
<meta name="keywords"
	content="2017年到2018年动作片高分排行榜,2017年到2018年动作片迅雷下载,2017年到2018年动作片热门电影BT下载,彬彬影视">
<meta name="description" content="2017年到2018年动作片高分排行榜  - 彬彬影视">
<link rel="miphtml" href="https://mip.btbuluo.com/rank-5.html">
<meta http-equiv="X-UA-COMPATIBLE" content="IE=edge">
<link rel="shortcut icon" href="${pageContext.request.contextPath }/images/favicon.png">
<link rel="apple-touch-icon-precomposed"
	href="${pageContext.request.contextPath }/images/logo/180.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="${pageContext.request.contextPath }/images/logo/72.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="${pageContext.request.contextPath }/images/logo/114.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="${pageContext.request.contextPath }/images/logo/144.png">
<link rel="apple-touch-icon-precomposed" sizes="180x180"
	href="${pageContext.request.contextPath }/images/logo/180.png">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath }/css/css.csss.css" />
</head>
<body>
	<header class="b2f55202bef67eabade" id="b2f55202bef67eabade">
		<a class="b287ce614443" ><img
			src="${pageContext.request.contextPath }/images/logo.svg"></a>
	</header>
	<div class="ba0593f22754f9f3ea9" id="ba0593f22754f9f3ea9">
		<div class="b776f355" id="b776f355">
			<span></span><span></span><span></span>
		</div>
	</div>
	<div class="bccc164d9066">
		<a ><i class="bc62 czs-home-l"></i></a>
	</div>
	<div class="bd049877" id="bd049877">
		<a class="b7e55" href="${pageContext.request.contextPath }/index_index.action"><img src="${pageContext.request.contextPath }/images/logo.svg"
			alt="b7e55"></a>
		<h1>彬彬影视</h1>
		<div class="ba1734c0f096f7d77">MRBEARD.CLUB</div>
		<div class="b9af650cd be38d4e28d7 ">
			<a href="${pageContext.request.contextPath }/index_index.action"><i class="b3d7 czs-home-l"></i><span>首页</span></a>
		</div>
		<div class="b9af650cd be38d4e28d7 ">
			<a href="${pageContext.request.contextPath }/movie_index.action"><i class="b3d7 czs-film-l"></i><span>电影</span></a>
		</div>
		<div class="b9af650cd b8e1d863ef0 ">
			<a href="${pageContext.request.contextPath }/tv_index.action"><i class="b3d7 czs-board-l"></i><span>电视剧</span></a>
		</div>
		<div class="b9af650cd bf36b72a370 be926ed">
			<a href="${pageContext.request.contextPath }/animation_index.action"><i class="b3d7 czs-bar-chart-l"></i><span>动漫</span></a>
		</div>
		<div class="b9af650cd bf36b72a370 ">
			<a href="${pageContext.request.contextPath }/variety_index.action"><i class="b3d7 czs-bookmark-l"></i><span>综艺</span></a>
		</div>
		<a class="b0f61695f1" href="${pageContext.request.contextPath }/index_index.action">&#169; MRBEARD.CLUB</a>
	</div>
	<div class="bbf8237161">
		<div class="be541">
			<div class="b8aa60 b3eba9f9970ec">
				<div class="bee3f7c2a">
					<i class="czs-circle"></i><span class="b76d563febbabc0b">选择播放器</span>
				</div>
				
			</div>
			<div class="b6c1">
				<div class="bc3f9 b7572 beb2e">
					<a href="http://jx.vipjiexi360.com/youku/apiget.php?url=${url }" class="bc943f67f b2b3da7b96848cd2 ">穷二代播放器</a>
				</div>
				<div class="bc3f9 b7572 beb2e">
					<a href="http://v.buy360.vip/cxjx.php?v=${url }" class="bc943f67f b4ad4f794fab49bc ">学习播放器</a>
				</div>
				<div class="bc3f9 b7572 beb2e">
					<a href="http://mlxztz.com/player.php?url=${url }" class="bc943f67f bbf0d277aec2ead6 ">神游播放器</a>
				</div>
				<div class="bc3f9 b7572 beb2e">
					<a href="http://mlxztz.com/dy2.php?url=${url }" class="bc943f67f b2b3da7b96848cd2 ">富二代播放器</a>
				</div>
				<div class="bc3f9 b7572 beb2e">
					<a href="http://mlxztz.com/dy/jiexl.php?url=${url }" class="bc943f67f b4ad4f794fab49bc  ">太虚播放器</a>
				</div>
				
			</div>
			<div class="ba55b065d7d">
			</div>
			<div class="b5cc3cc2ad85f">
	
					
				
			</div>
			<style></style>
			<footer class="b0dd420">
				<div class="ba7aa769d1d72112bb"></div>
				Copyright &#169; 2016-2018 彬彬影视 <a
					href="http://www.miitbeian.gov.cn/" rel="nofollow"
					style="color: #bbb; text-decoration: none;" target="_blank"
					rel="nofollow">琼ICP备16003139号-3</a>&nbsp;&nbsp;&nbsp;邮箱：hbz#foxmail.com
			</footer>
		</div>
	</div>
	<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath }/js/js.js"></script>
	<script data-no-instant>InstantClick.init();</script>
	<script data-no-instant>var _hmt = _hmt || [];(function() {var hm = document.createElement("script");hm.src = "https://hm.baidu.com/hm.js?cff4119c82a206302d3f47df2c74d907";var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(hm, s);})();</script>
	<script>(function(){var c=document.createElement("script");var b=window.location.protocol.split(":")[0];if(b==="https"){c.src="https://zz.bdstatic.com/linksubmit/push.js"}else{c.src="http://push.zhanzhang.baidu.com/push.js"}var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(c,a)})();</script>
</body>
</html>

